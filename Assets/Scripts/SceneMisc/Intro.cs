using UnityEngine;
using ButtonControls;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class Intro : MonoBehaviour
{
    [SerializeField] AudioClip _scaryMusic;
    private const int clicks = 2;

    private int _currentClick = 0;
    private Animator _anim;
    private AudioSource _aSource;


    private void Start()
    {
        _anim = GetComponent<Animator>();
        _aSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (ControlButtons.ThrowPress)
            if (_currentClick < clicks)
            {
                _currentClick++;
                _anim.SetTrigger("Next");
                if (_currentClick == 1)
                {
                    _aSource.clip = _scaryMusic;
                    _aSource.Play();
                }                    
            }
            else
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }

}
