using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Outro : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] private Sprite[] _scenes;

    private Image _image;
    private int _count = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        ++_count;
        if (_count < _scenes.Length)
            _image.sprite = _scenes[_count];
        else
            SceneManager.LoadScene(0);
    }

    void Start()
    {
        _image = GetComponent<Image>();
    }
}
