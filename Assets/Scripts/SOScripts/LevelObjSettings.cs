using UnityEngine;

[CreateAssetMenu(fileName = "LevelSettings", menuName = "ScriptableObjects/Level", order = 2)]
public class LevelObjSettings : ScriptableObject
{
    [Header("Coeff Increase")]
    public int IncreaseCoeff;
    public int DeathCoeff;

    [Header("FirePush")]
    public float PushPower;

    [Header("CoeffDecrease")]
    public int DecreaseCoeff;

    [Header("Inflammable")]
    public float TimeToBurn;
    public Color BurnColor;

    [Header("Straw")]
    public float HealAmount;

    [Header("Wheels")]
    public GameObject Wheel;
    public float TimeToSpawn;
}
