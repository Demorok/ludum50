using UnityEngine;

[CreateAssetMenu(fileName = "playerSettings", menuName = "ScriptableObjects/Player", order = 1)]
public class PlayerSettings : ScriptableObject
{
    [Header("PlayerMovement")]
    public float Speed;
    public float SpeedDamping;
    public float JumpForce;
    public float ExtraJump;
    public float SpeedThreshold;

    [Header("HealthManagement")]
    public float MaxHealth;
    public float MinBurnSpeed;
    public float MaxBurnSpeed;

    [Header("Thrower")]
    public float DamageOnShot;
    public float ShootPower;

}
