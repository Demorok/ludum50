using UnityEngine;

[CreateAssetMenu(fileName = "Sounds", menuName = "ScriptableObjects/Sound", order = 3)]
public class SoundSettings : ScriptableObject
{
    [Header("Gameplay Music")]
    public AudioClip MainTheme;

    [Header("Player")]
    public AudioClip Landing;
    public AudioClip ExtraJump;
    public AudioClip Throw;
    public AudioClip BurnOnDeath;
    public AudioClip Heal;
    public AudioClip Cool;
    public AudioClip Hot;
    public AudioClip Checkpoint;

    [Header("Inflammable")]
    public AudioClip Burn;

}
