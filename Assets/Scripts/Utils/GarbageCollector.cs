using UnityEngine;

public class GarbageCollector : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == Engine.PlayerLayer)
            Engine.Current.Restart();
        else
            Destroy(collision.gameObject);
    }
}
