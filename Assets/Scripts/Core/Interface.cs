using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour
{
    [SerializeField] private GameObject[] _fireImages;
    [SerializeField] public Image HpBar;
    public static Interface Current { get; private set; }

    private void Awake()
    {
        Current = this;
    }

    public void Change_Coeff(int coeff)
    {
        for (int i = 0; i < _fireImages.Length; i++)
        {
            if (i < coeff)
                _fireImages[i].SetActive(true);
            else
                _fireImages[i].SetActive(false);
        }
    }
}
