using System;
using UnityEngine;

namespace ButtonControls
{
    public static class ControlButtons
    {
        public static bool LeftPress
        {
            get
            {
                return Input.GetKeyDown(KeyCode.A);
            }
        }

        public static bool RightPress
        {
            get
            {
                return Input.GetKeyDown(KeyCode.D);
            }
        }

        public static bool ThrowPress
        {
            get
            {
                return Input.GetKeyDown(KeyCode.Mouse0);
            }
        }

        public static bool JumpPress
        {
            get
            {
                return Input.GetKeyDown(KeyCode.Space);
            }
        }

        public static bool RestartPress
        {
            get
            {
                return Input.GetKeyDown(KeyCode.R);
            }
        }

        public static bool LeftHold
        {
            get
            {
                return Input.GetKey(KeyCode.A);
            }
        }

        public static bool RightHold
        {
            get
            {
                return Input.GetKey(KeyCode.D);
            }
        }

        public static bool ThrowHold
        {
            get
            {
                return Input.GetKey(KeyCode.Mouse0);
            }
        }

        public static bool JumpHold
        {
            get
            {
                return Input.GetKey(KeyCode.Space);
            }
        }

        public static bool RestartHold
        {
            get
            {
                return Input.GetKey(KeyCode.R);
            }
        }

        public static int xAxis
        {
            get
            {
                return -Convert.ToInt32(LeftHold) + Convert.ToInt32(RightHold);
            }
        }
    }
}
