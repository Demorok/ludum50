using UnityEngine;
using Cinemachine;
using System;
using UnityEngine.UI;
using AnimationUtils.ImageUtils;
using UnityEngine.SceneManagement;

public class Engine : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    [SerializeField] private Transform _playerSpawn;
    [SerializeField] private CinemachineVirtualCamera _mainCamera;
    [SerializeField] private Image _curtain;

    [Header("Settings")]
    public PlayerSettings PlayerSettings;
    public LevelObjSettings LevelSettings;
    public SoundSettings SoundSettings;

    private static Vector3 _checkPoint;
    public bool Retry { get; private set; }
    private const float timeToFade = 0.2f;

    public static Engine Current { get; private set; }
    public ScareCrow Player { get; private set; }
    public Burning PlayerHealth { get; private set; }

    public const string InflammableTag = "Inflammable";
    public const int PlayerLayer = 6;

    public const string DissolveMaterialFade = "_Fade";
    public const string DissolveMaterialColor = "_Color";

    private void Awake()
    {
        Current = this;
        Retry = false;
        SoundRecorder.Play_Music(SoundSettings.MainTheme);
        if(_checkPoint == null)
            _checkPoint = _playerSpawn.position;
        Spawn_Character();
    }
    public void Spawn_Character()
    {
        GameObject _clone = Instantiate(_player, _checkPoint, Quaternion.identity);
        Player = _clone.GetComponent<ScareCrow>();
        PlayerHealth = _clone.GetComponent<Burning>();
        _mainCamera.Follow = _clone.transform;
    }

    public void SetCheckPoint(Transform checkPoint)
    {
        _checkPoint = checkPoint.position;
    }

    public void Hide_Scene(Action onComplete = null)
    {
        _curtain.gameObject.SetActive(true);
        _curtain.Unfade(timeToFade, onComplete);
    }

    public void Show_Scene(Action onComplete = null)
    {
        onComplete += () => _curtain.gameObject.SetActive(false);
        _curtain.Fade(timeToFade, onComplete);
    }

    public void Restart()
    {
        Retry = true;
        Hide_Scene(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
    }
}
