using ButtonControls;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class ScareCrow : MonoBehaviour
{
    private Collider2D _collider;
    private Rigidbody2D _rigidbody;
    private bool airborne = true;
    private bool _extraJump;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Move(int directionX)
    {
        if (directionX < 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else if (directionX > 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);

        int _directionX = _extraJump ? 0 : directionX;
        Vector2 _velocity = _rigidbody.velocity;
        _rigidbody.AddForce(Vector2.right * _directionX * Engine.Current.PlayerSettings.Speed, ForceMode2D.Force);
        if(directionX == 0)
            _velocity.x = _velocity.x > 0 ? _velocity.x - Mathf.Min(Engine.Current.PlayerSettings.SpeedDamping * Time.deltaTime, _velocity.x) 
                : _velocity.x + Mathf.Min(Engine.Current.PlayerSettings.SpeedDamping * Time.deltaTime, -_velocity.x);
        _velocity.x = Mathf.Clamp(_velocity.x, -Engine.Current.PlayerSettings.SpeedThreshold, Engine.Current.PlayerSettings.SpeedThreshold);
        _rigidbody.velocity = _velocity;
    }

    private void Jump()
    {
        if (!airborne && !ControlButtons.JumpHold)
        {
            _rigidbody.AddForce(Vector2.up * (Engine.Current.PlayerSettings.JumpForce + (_extraJump ? Engine.Current.PlayerSettings.ExtraJump : 0)), ForceMode2D.Impulse);
            airborne = true;
            SoundRecorder.Play_Effect(Engine.Current.SoundSettings.ExtraJump);
            _extraJump = false;
        }
        else if (!airborne && ControlButtons.JumpHold)
        {
            _extraJump = true;
            _rigidbody.velocity = Vector2.zero;
        }
    }

    private void Update()
    {
        if (!Engine.Current.Retry)
        {
            Move(ControlButtons.xAxis);
            Jump();
        }
        if (ControlButtons.RestartPress && !Engine.Current.Retry)
            Engine.Current.Restart();
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        CheckGround();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        CheckGround();
    }

    private void CheckGround()
    {
        Collider2D _hit = Physics2D.OverlapCircle(new Vector2(_collider.bounds.center.x, _collider.bounds.min.y), Physics2D.defaultContactOffset*2, LayerMask.GetMask("Ground"));
        if (!_hit)
            airborne = true;
        else
        {
            SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Landing);
            airborne = false;
        }
            
    }
}
