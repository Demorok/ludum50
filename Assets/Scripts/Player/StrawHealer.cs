using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Inflammable))]
public class StrawHealer : MonoBehaviour
{
    void Start()
    {
        GetComponent<Inflammable>().Burn();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
