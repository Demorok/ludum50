using UnityEngine;
using ButtonControls;

public class StrawThrower : MonoBehaviour
{
    [SerializeField] private GameObject _projectile;
    [SerializeField] private Transform _firePoint;

    private Burning _playerHealth;
    private Camera _mainCam;

    private void Start()
    {
        _mainCam = Camera.main;
        _playerHealth = GetComponent<Burning>();
    }
    void Update()
    {
        if (ControlButtons.ThrowPress)
            Shoot();
    }

    private void Shoot()
    {
        _playerHealth.Damage(Engine.Current.PlayerSettings.DamageOnShot);
        Rigidbody2D _clonerb = Instantiate(_projectile, _firePoint.position, Quaternion.identity).GetComponent<Rigidbody2D>();
        SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Throw);
        Vector3 direction = _mainCam.ScreenToWorldPoint(Input.mousePosition);
        direction.z = 0;
        direction -= _firePoint.position;
        direction = direction.normalized;
        _clonerb.AddForce(direction * Engine.Current.PlayerSettings.ShootPower, ForceMode2D.Impulse);
    }
}
