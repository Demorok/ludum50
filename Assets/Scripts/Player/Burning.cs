using UnityEngine;

public class Burning : MonoBehaviour
{
    [SerializeField] private GameObject _onDeath;

    private const int MaxCoeff = 4;

    private float _currentBurnSpeed;
    private float _currentHealth;
    private float _burnSpeedDifference;
    private int _coeff = 1;

    private void Start()
    {
        _burnSpeedDifference = Engine.Current.PlayerSettings.MaxBurnSpeed - Engine.Current.PlayerSettings.MinBurnSpeed;
        _currentBurnSpeed = Engine.Current.PlayerSettings.MinBurnSpeed;
        _currentHealth = Engine.Current.PlayerSettings.MaxHealth;
    }

    private void Update()
    {
        _currentBurnSpeed = Engine.Current.PlayerSettings.MinBurnSpeed + _burnSpeedDifference * (1 - _currentHealth / Engine.Current.PlayerSettings.MaxHealth);
        _currentHealth -= _currentBurnSpeed * Time.deltaTime * _coeff;
        Interface.Current.HpBar.fillAmount = _currentHealth / Engine.Current.PlayerSettings.MaxHealth;
        Check_Health();
    }

    public void IncreaseBurningCoeff(int incCoeff)
    {
        _coeff = Mathf.Min(MaxCoeff, _coeff + incCoeff);
        Interface.Current.Change_Coeff(_coeff);
    }

    public void DecreaseBurningCoeff(int decCoeff)
    {
        _coeff = Mathf.Max(1, _coeff - decCoeff);
        Interface.Current.Change_Coeff(_coeff);
    }

    public void Heal(float health = 0)
    {
        if (health == 0)
            _currentHealth = Engine.Current.PlayerSettings.MaxHealth;
        else
            _currentHealth = Mathf.Min(Engine.Current.PlayerSettings.MaxHealth, _currentHealth + health);
    }

    public void Damage(float health)
    {
        _currentHealth -= health;
    }

    public void Check_Health()
    {
        if (_currentHealth <= 0)
            Destroy(gameObject);
    }

}
