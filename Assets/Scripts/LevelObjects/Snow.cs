using UnityEngine;

public class Snow : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Engine.Current.PlayerHealth.DecreaseBurningCoeff(Engine.Current.LevelSettings.DecreaseCoeff);
        SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Cool);
        Destroy(gameObject);
    }
}
