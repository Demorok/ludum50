using UnityEngine;

public class Fire : MonoBehaviour
{
    private Collider2D _collider;

    private void Start()
    {
        _collider = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Heat(collision.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Heat(collision.gameObject);
    }

    private void Heat(GameObject obj)
    {
        if (obj.layer != Engine.PlayerLayer)
            return;
        Engine.Current.PlayerHealth.IncreaseBurningCoeff(Engine.Current.LevelSettings.IncreaseCoeff);
        if (obj.TryGetComponent(out Rigidbody2D playerrb))
        {
            SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Hot);
            Vector2 direction = (obj.transform.position - _collider.bounds.center);
            direction = direction.x > 0 ? Vector2.right : Vector2.left;
            playerrb.AddForce(direction * Engine.Current.LevelSettings.PushPower, ForceMode2D.Impulse);
        }
    }
}
