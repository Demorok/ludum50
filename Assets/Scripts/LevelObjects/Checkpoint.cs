using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Checkpoint);
        Engine.Current.SetCheckPoint(transform);
        Engine.Current.PlayerHealth.Heal();
        Engine.Current.PlayerHealth.DecreaseBurningCoeff(4);
        GetComponent<Collider2D>().enabled = false;
    }
}
