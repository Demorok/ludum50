using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Inflammable : MonoBehaviour
{
    [SerializeField] private LevelObjSettings _settings;
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private Shader _shader;
    public bool Burning { get; private set; }
    private Material _dissolveMat;
    private void Awake()
    {
        _dissolveMat = new Material(_shader);
        _renderer.material = _dissolveMat;
        _dissolveMat.SetFloat(Engine.DissolveMaterialFade, 1);
        _dissolveMat.SetColor(Engine.DissolveMaterialColor, _settings.BurnColor);
    }

    public void Burn()
    {
        if (!Burning)
            StartCoroutine(Incineration());
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Engine.InflammableTag))
            if (collision.gameObject.GetComponent<Inflammable>().Burning)
                Burn();
        if (collision.gameObject.layer == Engine.PlayerLayer)
            Burn();
    }

    IEnumerator Incineration()
    {
        float _startTime = Time.time;
        SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Burn);
        Burning = true;
        while(Time.time - _startTime < _settings.TimeToBurn)
        {
            _dissolveMat.SetFloat(Engine.DissolveMaterialFade, 1 - (Time.time - _startTime) / _settings.TimeToBurn);
            yield return null;
        }
        Destroy(gameObject);
    }
}
