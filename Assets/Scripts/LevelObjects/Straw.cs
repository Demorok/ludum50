using UnityEngine;

public class Straw : MonoBehaviour
{
    [SerializeField] LevelObjSettings _settings;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Engine.Current.PlayerHealth.Heal(_settings.HealAmount);
        SoundRecorder.Play_Effect(Engine.Current.SoundSettings.Heal);
        Destroy(gameObject);
    }
}
