using System.Collections;
using UnityEngine;

public class WheelSpawner : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(Spawner());
    }

    private IEnumerator Spawner()
    {
        while (true)
        {
            Instantiate(Engine.Current.LevelSettings.Wheel, transform);
            yield return new WaitForSeconds(Engine.Current.LevelSettings.TimeToSpawn);
        }
    }
}
